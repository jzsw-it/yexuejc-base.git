yexuejc-base 更新记录
------------------

#### version ：1.5.3-jre11
**time： ** <br/>
**branch：** jre11    <br/>
**update：**     <br/>
1. [FileUtil](src/main/java/com/yexuejc/base/util/FileUtil.java) 增加读取大文件自定义方法和单纯读取方法
2. [JsonUtil](src/main/java/com/yexuejc/base/util/JsonUtil.java) 增加objToMap；优化obj2Json
3. [DateUtil](src/main/java/com/yexuejc/base/util/DateUtil.java) 标准化日期时间的转换函数
4. [AES](src/main/java/com/yexuejc/base/encrypt/AES.java) 兼容ECB(虽然不再建议利用)
5. [StrUtil](src/main/java/com/yexuejc/base/util/StrUtil.java) 增加国家代码二进制相互转换
---

#### version ：1.5.2-jre11
**time：2024-4-7 14:34:33** <br/>
**branch：** jre11    <br/>
**update：**     <br/>
1. 升级相关依赖
2. 依赖工具读取文件[FileInput.java](src/main/java/com/yexuejc/base/file/FileInput.java)从[FileUtil.java](src/main/java/com/yexuejc/base/util/FileUtil.java)中提取出来
3. 优化[FileUtil.java](src/main/java/com/yexuejc/base/util/FileUtil.java)
4. 优化[JwtUtil.java](src/main/java/com/yexuejc/base/util/JwtUtil.java)
---


#### version ：1.5.1-jre11
**time：2023-6-8 16:02:56** <br/>
**branch：** jre11    <br/>
**update：**     <br/>
1. FileUtil 增加读取csv文件（分页读取）
            提供读取IO流方法合集
2. 增加AES加解密
3. 增加文件压缩ZipUtil
4. DateUtil，DateTimeUtil 时间操作工具优化
5. JsonUtil [feat] 增加json化时对LocalDateTime,LocalDate,Timestamp时间的优化,增加特殊场景序列化反序列化



---
#### version ：1.5.0-jre8
**time：2022-5-9 13:37:31** <br/>
**branch：** master    <br/>
**update：**     <br/>
1. 升级jwt相关包，去除`io.jsonwebtoken:jjwt:0.9.1`（不再维护升级）,
取而代之的是引入以下包，升级相关依赖
```
io.jsonwebtoken:jjwt-api:0.11.5
io.jsonwebtoken:jjwt-impl:0.11.5
io.jsonwebtoken:jjwt-jackson:0.11.5
org.bouncycastle:bcprov-jdk15on:1.70
```
2. 升级
```
jakarta.validation:jakarta.validation-api:3.0.1
org.apache.poi:poi:5.2.2
com.google.guava:guava:31.1-jre
commons-io:commons-io:2.11.0
```
4. 去除依赖
```
<commons-codec.version>1.15</commons-codec.version>
<!--base64使用到的依赖-->
<dependency>
   <groupId>commons-codec</groupId>
   <artifactId>commons-codec</artifactId>
   <version>${commons-codec.version}</version>
   <scope>compile</scope>
</dependency>
   ```
#
#### version ：1.4.5
**time：2022-5-9 13:37:31** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 扩展FileUtil,优化Base64的包
#
#### version ：1.4.4
**time：2021-4-24 00:41:31** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. JsonUtil类修复格式化出现的时区差：现使用TimeZone.getDefault()时区
#
#### version ：1.4.3
**time：2021-2-6 11:42:49** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. DateTimeUtil类增加时间格式
#
#### version ：1.4.2
**time：2021-2-3 11:40:11** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化时间转换格式
>2. 增加注解 ToUeProperty 转换时间为时间戳的功能
#
#### version ：1.4.1
**time：2021-1-31 12:59:24** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 升级所有依赖
>1. 增加DateTimeUtil类的功能
#
#### version ：1.4.0
**time：2020-5-18 12:06:14** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 更新仓库地址 https://nexus.yexuejc.club/ 为 https://nexus.yexuejc.top/
>2. 优化Execl 和 Jwt 工具
#
#### version ：1.3.9
**time：2019-1-11 16:50:51** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. RSA2 增加证书格式转换 JKS(xx.keystore) 、 PKCS12(xx.pfx)相互转换
#

#### version ：1.3.8
**time：2019-1-11 13:28:12** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. [ToUeProperty](src/main/java/com/yexuejc/base/util/ToUeProperty.java) 增加 ignore
#

#### version ：1.3.7
**time：2019-1-11 10:02:03** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 考虑到向下兼容，回滚1.3.6【json增加下划线、驼峰互转】,考虑更优方案（解决加入下个版本）
#

#### version ：1.3.6
**time：2019-1-10 14:55:13** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. json增加下划线、驼峰互转
#
#### version ：1.3.5
**time：2019-1-7 17:19:22** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. objUtil 增加兼容类型
#
#### version ：1.3.4
**time：2019-1-2 20:32:12** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. objUtil list类型修复
#
#### version ：1.3.3
**time：2019-1-2 14:06:47** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. MoneyUtil 扩展元转分
#
#### version ：1.3.2
**time：2019-1-2 14:06:47** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. objUtil 枚举类型修复
#
#### version ：1.3.1
**time：2019-1-2 14:06:47** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. objUtil 增加类字段（驼峰）转换成下划线
#
#### version ：1.3.0
**time：2018-12-30 16:47:50** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 移交发布到maven中央仓库
>2. 移交后变更groupId 为`top.yexuejc`
>3. 源码发布由`成都极致思维网络科技有限公司`维护，github开源地址不变，gitee从组织[ICC(InCloudCode)](https://gitee.com/incloudcode)转移到[成都极致思维网络科技有限公司/yexuejc-base](https://gitee.com/jzsw-it/yexuejc-base)

#
#### version ：1.2.9
**time：2018-12-29 14:51:33** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 获取RSA密钥增加以输入流的形式获取密钥

#
#### version ：1.2.6
**time：2018-12-21 14:58:49** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. RSA 验签增加初始化方法

#
#### version ：1.2.8
**time：2018-12-28 20:10:14** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 新增[ObjUtil](src/main/java/com/yexuejc/base/util/ObjUtil.java) 对类（对象）进行处理，提供深度克隆

#
#### version ：1.2.6
**time：2018-12-21 14:58:49** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. RSA 验签增加初始化方法

#
#### version ：1.2.7
**time：2018-12-24 15:31:01** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. FileUtil增加base64转File `base64ToFile()`

#
#### version ：1.2.6
**time：2018-12-21 14:58:49** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. RSA 验签增加初始化方法

#
#### version ：1.2.5
**time：2018-12-20 13:13:23** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 丰富[JsonUtil](src/main/java/com/yexuejc/base/util/JsonUtil.java),支持直接对Map泛型转换

#
#### version ：1.2.4
**time：2018-11-27 14:46:04** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 工具类的优化
>2.规范代码

#
#### version ：1.2.3
**time：2018-11-23 16:45:42** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 修复RSA加密（签名）时选择的Base64（encodeBase64URLSafeString、encodeBase64String）区分
#
#### version ：1.2.1
**time：2018-11-9 15:05:06** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化resps
#
#### version ：1.2.2
**time：2018-11-20 20:20:12** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化RSA 加解密
>1. 增加RSA 签名
#
#### version ：1.2.1
**time：2018-11-9 15:05:06** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化resps
#
#### version ：1.2.0
**time：2018-10-19 11:38:20** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 增加异步线程处理工具
```$java
SysUtil.threadRun(() -> {
    //异步执行代码块
}
```
#

#### version ：1.1.9
**time：2018-9-23 11:57:36** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化工具类包名：不向下兼容，升级请修改
>2. 升级JWT工具类：更改为单例模式，可配置参数
#

#### version ：1.1.8
**time：2018-9-3 19:29:56** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 增肌图片处理工具类
>2. 增肌3des工具类
>3. 增肌RSA工具类
>4. 优化其他工具类
#
#### version ：1.1.7
**time：2018-8-17 11:22:50** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化ApiVO
#

#### version ：1.1.6
**time：2018-7-7 11:32:56** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. maven仓库更新
#

#### version ：1.1.5
**time：2018-6-19 22:16:34** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 优化ApiVO

#
#### version ：1.1.4
**time：2018-6-14 22:27:59** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 统一编码：UTF-8

#
#### version ：1.1.3
**time：2018年6月2日12:16:58** <br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 修改正则RegexUtils.java
>2. 修改正则StrUtil.java->扩展genUUID()

#
#### version ：1.1.2
**time：** 2018-5-16 15:03:28<br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 修改依赖

#
#### version ：1.1.1
**time：** 2018-5-12 22:25:05<br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 添加RSA
#

##### version ：1.1.0
**time：** 2018-5-12 22:25:05<br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 添加支持加密功能
#

#### version ：1.0.0
**time：** 2018-1-31 12:16:10<br/>
**branch：** master    <br/>
**update：**     <br/>
>1. 基于java8开发的web应用工具包
#