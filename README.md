yexuejc-base 基于jdk11常用工具包
----------------------
源码地址:<br>
github:https://github.com/yexuejc/yexuejc-base
gitee:https://gitee.com/jzsw-it/yexuejc-base

### 说明
1. 支持环境：java11（1.5.0开始支持java11，请使用`1.5.x-jre11`版本）
2. 该工具包基于springboot提取，按理说适用于所有java工程
7. 从`1.5.0`开始，版本分为`1.5.0-jre8`和`1.5.0-jre11`，分别对于jre8和jre11使用（后续逐渐放弃jre8）

### 使用

pom.xml
```
<dependencies>
    <dependency>
        <groupId>top.yexuejc</groupId>
        <artifactId>yexuejc-base</artifactId>
        <version>1.5.2-jre11</version>
    </dependency>
</dependencies>
```

### 工具文档
[Wiki](WIKI.md)

### 更新日志
[更新记录](UPDATE.md)

#### 项目发展
本工程项目由maxf基于日常使用，从[yexuejc-springboot](https://github.com/yexuejc/yexuejc-springboot.git)（_准备移交版本控制_）中抽离开源独立发展，后续增加许多常用工具包。
使用者逐渐增多后考虑可靠性和稳定性原则，移交版本控制给`成都极致思维网络科技有限公司`管理，maven包直接发布到中央仓库。
开源工程项目仍然保持继续维护和欢迎更多愿意贡献的小伙伴参与。
