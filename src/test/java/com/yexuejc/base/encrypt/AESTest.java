package com.yexuejc.base.encrypt;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class AESTest {

    @Test
    public void testEncrypt() throws Exception {
        String data = "Hello World!";
        AES aes = AES.builder()
                .setAlgorithm(AES.ALGORITHM.AES_CBC_PKCS5Padding)
                .setKey("hj7x89H$yuBI0456")
                .setIv("NIfb&95GUY86Gfgh")
                .setCharset(StandardCharsets.UTF_8);
        String encrypted = aes.encrypt(data);
        assertNotNull(encrypted);
        assertFalse(encrypted.isEmpty());
    }

    @Test
    public void testDecrypt() throws Exception {
        String data = "p0x0vK5T6OOy69+p9cgI/9xfeoi/f0t6NO7HbLsUON4=";
        AES aes = AES.builder()
                .setAlgorithm(AES.ALGORITHM.AES_CBC_PKCS5Padding)
                .setKey("hj7x89H$yuBI0456")
                .setIv("NIfb&95GUY86Gfgh")
                .setCharset(StandardCharsets.UTF_8);
        String decrypted = aes.decrypt(data);
        assertNotNull(decrypted);
        assertFalse(decrypted.isEmpty());
        assertEquals("Hello World!", decrypted);
    }

    @Test
    public void testEncryptAndDecrypt() throws Exception {
        String data = "张三";
        AES aes = AES.builder()
                .setAlgorithm(AES.ALGORITHM.AES_OFB_ISO10126Padding)
                .setKey("hj7x89H$yuBI0456")
                .setIv("NIfb&95GUY86Gfgh")
                .setCharset(StandardCharsets.UTF_8);
        String encrypt = aes.encrypt(data);
        System.out.println("加密：" + encrypt);
        String decrypt = aes.decrypt(encrypt);
        System.out.println("解密：" + decrypt);
    }

}
