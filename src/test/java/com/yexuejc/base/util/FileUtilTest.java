package com.yexuejc.base.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import com.yexuejc.base.pojo.ReadFileBean;
import com.yexuejc.base.util.bean.AppnodeCertCsvBean;

/**
 *
 * @author: yexuejc
 * @date: 2024/4/8 11:33
 */
public class FileUtilTest {
    public static void main(String[] args) throws IOException {
        readCsvFile();
//        other();
    }

    private static void other() throws IOException {
        System.out.println(FileUtil.getFileType("C:\\Users\\Administrator\\Desktop\\test.txt"));
        boolean b = FileUtil.judeDirExists(new File("F:\\coding\\yexuejc-base2\\src\\test\\java\\com\\yexuejc\\base\\util\\test\\a"));
        File file = new File("F:\\coding\\yexuejc-base2\\src\\test\\java\\com\\yexuejc\\base\\util\\test\\a\\test.txt");
        FileUtil.judeFileExists(file);
        System.out.println("创建文件夹：" + b);
        System.out.println("SHA1:" + FileUtil.sha1(file));
        //超大文件sha1
        long l = System.currentTimeMillis();
        System.out.println("SHA1:" + FileUtil.sha1(Paths.get("F:\\Docker\\win\\win10x64.iso").toFile()) + "   花费时间：" + (System.currentTimeMillis() - l));

        System.out.println("MD5:" + FileUtil.md5(file));
        //超大文件MD5
        long l2 = System.currentTimeMillis();
        System.out.println("MD5:" + FileUtil.md5(Paths.get("F:\\Docker\\win\\win10x64.iso").toFile()) + "   花费时间：" + (System.currentTimeMillis() - l2));
        //超大文件MD5
        long l3 = System.currentTimeMillis();
        System.out.println("CRC32:" + FileUtil.crc32(Paths.get("F:\\Docker\\win\\win10x64.iso").toFile()) + "   花费时间：" + (System.currentTimeMillis() - l3));

        String base64ToStr = FileUtil.base64ToStr(file);
        System.out.println(base64ToStr);
        String fileName = "F:\\coding\\yexuejc-base2\\src\\test\\java\\com\\yexuejc\\base\\util\\test\\a\\test2.txt";
        System.out.println(FileUtil.base64ToFile(base64ToStr, fileName));
        File file2 = Paths.get(fileName).toFile();
        System.out.println("SHA1:" + FileUtil.sha1(file2));
        System.out.println("MD5:" + FileUtil.md5(file2));


        System.out.println(FileUtil.size(file2.toPath()));
        long l4 = System.currentTimeMillis();
        System.out.println(FileUtil.size(Paths.get("F:\\Docker\\win\\win10x64.iso")) + "   花费时间：" + (System.currentTimeMillis() - l4));

    }

    private static void readCsvFile() throws IOException {
        String path = "F:\\coding\\yexuejc-base\\src\\test\\java\\com\\yexuejc\\base\\util\\test.csv";

//        List<AppnodeCertCsvBean> list = FileUtil.readCsv(path, AppnodeCertCsvBean.class, true, "enable,domain,protocol,deployHost,deployPath,uname,pwd,appnodeId", ',');
//        System.out.println("***********************************************");
//        System.out.println(JsonUtil.formatPrinter(list));
//        System.out.println("条数：" + list.size());

        //直接把每行读取成字符串
        ReadFileBean<String> readFileBean2 = new ReadFileBean<>(2);
        ReadFileBean<String> bean2 = FileUtil.readBigFile(path, readFileBean2);
        System.out.println("直接把每行读取成字符串============================================");
        System.out.println(JsonUtil.formatPrinter(bean2));
        System.out.println("直接把每行读取成字符串============================================");

        //自定义每行数据的处理
        ReadFileBean<AppnodeCertCsvBean> readFileBean1 = new ReadFileBean<>(2);
        ReadFileBean<AppnodeCertCsvBean> bean1 = FileUtil.readBigFile(path, readFileBean1, datas -> {
            if (readFileBean1.getStartRowNum() == 1) {
                datas.remove(0);//跳过第一行
            }
            return datas.stream().map(str -> {
                //自定义处理每一条数据
                String[] split = str.split(",");
                AppnodeCertCsvBean app = new AppnodeCertCsvBean();
                app.setEnable(getValue(split, 0));
                app.setDomain(getValue(split, 1));
                app.setProtocol(getValue(split, 2));
                app.setDeployHost(getValue(split, 3));
                app.setDeployPath(getValue(split, 4));
                app.setUname(getValue(split, 5));
                app.setPwd(getValue(split, 6));
                if (StrUtil.isNotEmpty(getValue(split, 7))) {
                    app.setAppnodeId(Integer.valueOf(getValue(split, 7)));
                }
                return app;
            }).collect(Collectors.toList());
        });
        System.out.println("自定义每行数据的处理============================================");
        System.out.println(JsonUtil.formatPrinter(bean1));
        System.out.println("自定义每行数据的处理============================================");

        //直接使用提供的csv文件读取
        ReadFileBean<AppnodeCertCsvBean> readFileBean = new ReadFileBean<>(2);
        do {
            ReadFileBean<AppnodeCertCsvBean> bean = FileUtil.readBigFile(path, readFileBean, AppnodeCertCsvBean.class);
            System.out.println("直接使用提供的csv文件读取============================================");
            System.out.println(JsonUtil.formatPrinter(bean));
            System.out.println("直接使用提供的csv文件读取============================================");
        } while (readFileBean.hasNext());

    }

    private static String getValue(String[] value, int index) {
        try {
            return value[index];
        } catch (Exception e) {
            return "";
        }
    }
}
