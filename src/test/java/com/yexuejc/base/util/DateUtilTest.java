package com.yexuejc.base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateUtilTest {

    @Test
    public void testParseDate() throws ParseException {
        String dateStr = "2022-03-20";
        String dateFormat = "yyyy-MM-dd";
        Date date = DateUtil.parseDate(dateStr, dateFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        assertEquals(sdf.format(date), dateStr);
    }

    @Test
    public void testParseLocalDate() throws ParseException {
        String dateStr = "2022-03-20";
        String dateFormat = "yyyy-MM-dd";
        LocalDate date = DateTimeUtil.parseLocalDate(DateUtil.parseDate(dateStr, dateFormat));
        assertEquals(date.toString(), dateStr);
    }

    @Test
    public void testFormatDateNow() {
        String dateFormat = "yyyy-MM-dd";
        String nowDateStr = DateUtil.formatDateNow(dateFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String nowDate = sdf.format(new Date());
        assertEquals(nowDate, nowDateStr);
    }

    @Test
    public void testFormatDate() {
        Date date = new Date();
        String dateFormat = "yyyy-MM-dd";
        String formattedDate = DateUtil.formatDate(date, dateFormat);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        assertEquals(sdf.format(date), formattedDate);
    }

    @Test
    public void testFormatDateWithLocale() {
        Date date = new Date();
        String dateFormat = "yyyy-MM-dd";
        Locale locale = Locale.JAPAN;
        String formattedDate = DateUtil.formatDate(date, dateFormat, locale);
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, locale);
        assertEquals(sdf.format(date), formattedDate);
    }

    @Test
    public void testGetDateByPlusDay() {
        int days = 2;
        String dateFormat = "yyyy-MM-dd";
        String plusDateStr = DateUtil.formatDate(DateUtil.datePlus(new Date(), days), dateFormat);
        LocalDate nowDate = LocalDate.now();
        LocalDate plusDate = nowDate.plusDays(days);
        assertEquals(plusDate.toString(), plusDateStr);
    }

}
