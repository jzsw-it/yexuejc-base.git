package com.yexuejc.base.util.bean;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.yexuejc.base.annotation.CsvToBean;
import com.yexuejc.base.converter.IntegerNullValueDeserializer;

/**
 *
 * @author: yexuejc
 * @date: 2024/2/27 10:40
 */
@CsvToBean(header = "enable,domain,protocol,deployHost,deployPath,uname,pwd,appnodeId")
public class AppnodeCertCsvBean implements Serializable {
    /**是否生效：Y/N*/
    private String enable;
    /**域名*/
    private String domain;
    /**
     * 部署协议
     * appnode
     * ssh
     *  */
    private String protocol;
    /**
     * 部署服务器
     * 部署协议appnode: local 本机部署
     * 部署协议appnode: 域名 部署的远程appnode域名
     * 部署协议ssh : IP
     * */
    private String deployHost;
    /**部署证书位置*/
    private String deployPath;
    /**服务器账号*/
    private String uname;
    /**服务器密码*/
    private String pwd;
    /**
     * appnode协议时：且远程部署时，对应的远程appnode的ApiNodeId
     */
    @JsonDeserialize(using = IntegerNullValueDeserializer.class)
    private Integer appnodeId;

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getDeployHost() {
        return deployHost;
    }

    public void setDeployHost(String deployHost) {
        this.deployHost = deployHost;
    }

    public String getDeployPath() {
        return deployPath;
    }

    public void setDeployPath(String deployPath) {
        this.deployPath = deployPath;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getAppnodeId() {
        return appnodeId;
    }

    public void setAppnodeId(Integer appnodeId) {
        this.appnodeId = appnodeId;
    }
}
