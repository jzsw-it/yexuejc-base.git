package com.yexuejc.base.util;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonUtilTest {
    @Test
    public void test2() {
//        TestA testA = new TestA("张三", 1, false);
//        List<TestA> list = new ArrayList<>();
//        list.add(testA);
//        list.add(testA);
//        Map<String, List<TestA>> data = new HashMap<>();
//        data.put("data",list);
//        System.out.println(JsonUtil.obj2Json(data));
//        System.out.println("===========================");
        String readData="{\"data\":[{\"name\":\"张三\",\"id\":1,\"sex\":false},{\"name\":\"张三\",\"id\":1,\"sex\":false}]}";
        Map<String, List<TestA>> map = JsonUtil.json2Obj(readData, new TypeReference<Map<String, List<TestA>>>() {
        });
        System.out.println(JsonUtil.obj2Json(map));
        List<TestA> testAS = map.get("data");
        System.out.println(testAS.getClass().toString());
        System.out.println(testAS.get(0).getClass().toString());
    }

    @Test
    public void test1() {
        TestA testA = new TestA("张三", 1, false);
        System.out.println(JsonUtil.json2Obj(JsonUtil.obj2Json(testA), Map.class));

        List<TestA> list = new ArrayList<>();
        list.add(testA);
        list.add(testA);
        list.add(testA);
        System.out.println(JsonUtil.json2Obj(JsonUtil.obj2Json(list), List.class, TestA.class));
    }

    static class TestA implements Serializable {
        public String name;
        public Integer id;
        public Boolean sex;

        public TestA() {
        }

        public TestA(String name, Integer id, Boolean sex) {
            this.name = name;
            this.id = id;
            this.sex = sex;
        }
    }
}