package com.yexuejc.base.util;

import java.util.Map;

import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;

/**
 * @author maxiaofeng
 * @date 2024/9/1 15:23
 */
class JwtUtilTest {

    @Test
    void compact() {
        Map map = JsonUtil.json2Obj("{\n" + "    \"payload\": {\n" + "        \"sub\": \"0fqgx9h1d9eijd07umlyn1ez52jvkkkb883j\",\n" + "        " +
                "\"iss\": \"www" + ".donki.com\",\n" + "        \"aud\": \"D27A80C9AF01488F9A1152B8C20D93BA\",\n" + "        \"exp\": 1725260806," +
                "\n" + "        " + "\"iat\": 1725174406,\n" + "        \"address\": {\n" + "            \"country\": \"南朝鮮\"\n" + "        },\n" + "        " + "\"email_verified\": true,\n" + "        \"birthdate\": \"1993/11/16\",\n" + "        \"news_letter\": \"1\",\n" + "        " + "\"gender\": \"other\",\n" + "        \"member_code\": \"3511623793483553889787\",\n" + "        \"preferred_username\": " + "\"3923490434\",\n" + "        \"nonce\": \"mjhjSTmtVRNWPBtOQXZx\",\n" + "        \"country_code\": \"CHN\",\n" + "        " + "\"language_code\": \"zh-TW\",\n" + "        \"updated_at\": 1722404351,\n" + "        \"nickname\": \"3923490434\",\n" + "        " + "\"customer_flag\": \"0\",\n" + "        \"service_term_version\": \"2\",\n" + "        \"email\": \"1828844769@qq.com\",\n" + "   " + "     \"auth_time\": 1725173983\n" + "    }\n" + "}", Map.class);
        JwtUtil jwtUtil = JwtUtil.config("JWT", "www.donki.com", "5326D79EDEF344C3B3431F268DD2564C", Jwts.SIG.HS256);
        String jwtStr = jwtUtil.compact(map.get("payload"));
        System.out.println(jwtStr);

        Map<?, ?> parse = jwtUtil.parse(jwtStr);
        System.out.println(JsonUtil.obj2Json(parse));
    }
}