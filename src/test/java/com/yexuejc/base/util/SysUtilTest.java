package com.yexuejc.base.util;

/**
 *
 * @author: yexuejc
 * @date: 2024/4/8 11:22
 */
public class SysUtilTest {
    public static void main(String[] args) {
        System.out.println(SysUtil.getCachePath());
        System.out.println(SysUtil.getRootPath(SysUtilTest.class, null));
        SysUtil.threadRun("test", () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("当前线程的名称是：" + threadName);
        });
        SysUtil.getThreadList().forEach(t -> {
            System.out.println("线程名称：" + t.getName());
        });
        SysUtil.checkJvmMemory();
        System.out.println(SysUtil.jvmMemoryIsNotExecutable());
    }
}
