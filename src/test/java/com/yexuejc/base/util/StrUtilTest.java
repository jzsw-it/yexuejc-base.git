package com.yexuejc.base.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StrUtilTest {

    @Test
    void isEmpty() {
    }

    @Test
    void isNotEmpty() {
    }

    @Test
    void genUUID() {
    }

    @Test
    void testGenUUID() {
    }

    @Test
    void genNum() {
    }

    @Test
    void toHex() {
    }

    @Test
    void toMD5() {
    }

    @Test
    void toSHA256() {
    }

    @Test
    void toSHA() {
    }

    @Test
    void iso2utf() {
    }

    @Test
    void isNumeric() {
    }

    @Test
    void codeId() {
    }

    @Test
    void decodeId() {
    }

    @Test
    void parseUrlencoded() {
    }

    @Test
    void getSignContent() {
    }

    @Test
    void replaceMobile() {
    }

    @Test
    void mapSort() {
    }

    @Test
    void setStr() {
    }

    @Test
    void underlineToCamel() {
    }

    @Test
    void camelToUnderline() {
    }

    @Test
    void printStackTraceAndMessage() {
    }

    @Test
    void printStackTrace() {
    }

    @Test
    void notNullExecute() {
    }

    @Test
    void notEmptyExecute() {
    }

    @Test
    void countryToCode() {
        Assertions.assertEquals(StrUtil.countryToCode("JPN"), "100000000");
        Assertions.assertEquals(StrUtil.countryToCode("KOR"), "010000000");
        Assertions.assertEquals(StrUtil.countryToCode("THA"), "001000000");
        Assertions.assertEquals(StrUtil.countryToCode("SGP"), "000100000");
        Assertions.assertEquals(StrUtil.countryToCode("CHN"), "000010000");
        Assertions.assertEquals(StrUtil.countryToCode("TWN"), "000001000");
        Assertions.assertEquals(StrUtil.countryToCode("HKG"), "000000100");
        Assertions.assertEquals(StrUtil.countryToCode("MAC"), "000000010");
        Assertions.assertEquals(StrUtil.countryToCode("999"), "000000001");
        Assertions.assertEquals(StrUtil.countryToCode("O"), "000000000");

    }

    @Test
    void getCountryByCode() {
        Assertions.assertEquals(StrUtil.getCountryByCode("100000000"), "JPN");
        Assertions.assertEquals(StrUtil.getCountryByCode("010000000"), "KOR");
        Assertions.assertEquals(StrUtil.getCountryByCode("001000000"), "THA");
        Assertions.assertEquals(StrUtil.getCountryByCode("000100000"), "SGP");
        Assertions.assertEquals(StrUtil.getCountryByCode("000010000"), "CHN");
        Assertions.assertEquals(StrUtil.getCountryByCode("000001000"), "TWN");
        Assertions.assertEquals(StrUtil.getCountryByCode("000000100"), "HKG");
        Assertions.assertEquals(StrUtil.getCountryByCode("000000010"), "MAC");
        Assertions.assertEquals(StrUtil.getCountryByCode("000000001"), "999");
        Assertions.assertEquals(StrUtil.getCountryByCode("000000000"), "O");
        Assertions.assertEquals(StrUtil.getCountryByCode("100000000000"), "O");
        Assertions.assertEquals(StrUtil.getCountryByCode("-100000000000"), "O");
    }

    @Test
    void countryToCodeByte() {
        System.out.println(String.format("%9s", Integer.toBinaryString(StrUtil.countryToCodeByte("CHN") & 0xFF)).replace(" ", "0"));
    }
}