package com.yexuejc.base.util;

import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @ClassName: StrUtil
 * @Description:
 * @author: maxf
 * @date: 2018/5/12 19:13
 */
public final class StrUtil {
    private StrUtil() {
    }

    private static final char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * 判断字符串，数组，集合 是否为空(null，""，[],{})
     *
     * @param obj 対象
     * @return true:空;false:非空
     * @return
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof Optional) {
            return ((Optional<?>) obj).isEmpty();
        } else if (obj instanceof CharSequence) {
            return ((CharSequence) obj).length() == 0;
        } else if (obj.getClass().isArray()) {
            return Array.getLength(obj) == 0;
        } else if (obj instanceof Collection) {
            return ((Collection<?>) obj).isEmpty();
        } else {
            return obj instanceof Map && ((Map<?, ?>) obj).isEmpty();
        }
    }

    public static boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 生成32位UUID
     *
     * @return
     */
    public static String genUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 生成指定位数UUID
     *
     * @param length
     * @return
     */
    public static String genUUID(int length) {
        if (length <= 32) {
            return genUUID().substring(0, length);
        } else if (length < 1) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length / 32; i++) {
                sb.append(genUUID());
            }
            if (length % 32 > 0) {
                sb.append(genUUID(), 0, length % 32);
            }
            return sb.toString();
        }
    }

    /**
     * <p>生成11位编号，可以用作订单号，有很小几率出现重复，需要做异常处理</p>
     * <p>左边第一位为正负标识：正数1 负数0</p>
     * <p> 剩余位数为UUID的hashcode值</p>
     * <p>可以再生成的编号基础上嵌入其他标识编码</p>
     *
     * @return
     */
    public static String genNum() {
        int hashCode = UUID.randomUUID().toString().hashCode();
        StringBuilder num = new StringBuilder();
        if (hashCode < 0) {
            hashCode = -hashCode;
            num.append("0");
        } else {
            num.append("1");
        }
        return num.append(String.format("%010d", hashCode)).substring(0, 8);
    }

    /**
     * 字符串转换方法 把字节数组转换成16进制字符串
     *
     * @param buf 初始字节数组
     * @return 转换后字符串
     */
    public static String toHex(byte[] buf) {
        StringBuilder sb = new StringBuilder(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                sb.append("0");
            }
            sb.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return sb.toString();
    }

    /**
     * 获取字符串的MD5码
     *
     * @param str
     * @return
     */
    public static String toMD5(String str) {
        if (str == null) {
            return null;
        }
        MessageDigest md = null;
        try {
            md = java.security.MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        md.update(str.getBytes());
        byte[] tmp = md.digest();
        return toHex(tmp);
    }

    /**
     * SHA256加密
     *
     * @param str
     * @return
     */
    public static String toSHA256(final String str) {
        return toSHA(str, "SHA-256");
    }

    /**
     * SHA加密
     *
     * @param str
     * @param key
     * @return
     */
    public static String toSHA(final String str, final String key) {
        // 是否是有效字符串
        if (str == null) {
            return null;
        }
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(key);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
        messageDigest.update(str.getBytes());
        byte[] tmp = messageDigest.digest();
        return toHex(tmp);
    }

    /**
     * 用ISO-8859-1解码 再用UFT-8编码
     *
     * @param str
     * @return
     */
    public static String iso2utf(String str) {
        String utfStr = null;
        utfStr = new String(str.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
        return utfStr;
    }

    /**
     * 判断字符串是否是数字
     *
     * @param str
     * @return
     */
    private static final Pattern pattern = Pattern.compile("[0-9]*");

    public static boolean isNumeric(String str) {
        Matcher isNum = pattern.matcher(str);
        return isNum.matches();
    }

    /**
     * 对ID（32位）进行编码
     *
     * @param id 32位ID
     * @return 编码后的64位ID
     */
    public static String codeId(String id) {
        if (id == null || id.length() != 32) {
            return id;
        }

        StringBuilder coded = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 13; i++) {
            coded.append(HEX_CHAR[random.nextInt(16)]);
        }
        coded.append(id, 0, 11);
        for (int i = 0; i < 7; i++) {
            coded.append(HEX_CHAR[random.nextInt(16)]);
        }
        coded.append(id.substring(11));
        for (int i = 0; i < 12; i++) {
            coded.append(HEX_CHAR[random.nextInt(16)]);
        }

        return coded.toString();
    }

    /**
     * 对ID（32位）进行解码
     *
     * @param coded 编码后的64位ID
     * @return 解码后的32位ID
     */
    public static String decodeId(String coded) {
        if (coded == null || coded.length() != 64) {
            return coded;
        }

        StringBuilder id = new StringBuilder();
        id.append(coded, 13, 24);
        id.append(coded, 31, 52);

        return id.toString();
    }

    /**
     * 解析aa=bb&cc=dd&ee=ff格式的字符串，返回HashMap
     *
     * @param urlencoded
     * @return
     */
    public static Map<String, String> parseUrlencoded(String urlencoded) {
        if (isEmpty(urlencoded)) {
            return null;
        }
        String[] entrys = urlencoded.split("&");
        if (isEmpty(entrys)) {
            return null;
        }

        Map<String, String> map = new HashMap<>(16);
        String[] kv = null;
        for (String entry : entrys) {
            if (isEmpty(entry)) {
                continue;
            }
            kv = entry.split("=");
            if (isEmpty(kv)) {
                continue;
            }
            if (kv.length > 1) {
                map.put(kv[0], kv[1]);
            } else {
                map.put(kv[0], null);
            }
        }
        return map;
    }

    /**
     * map parameters 转url parameters
     *
     * @param sortedParams
     * @return
     */
    public static String getSignContent(Map<String, ?> sortedParams) {
        StringBuilder content = new StringBuilder();
        List<String> keys = new ArrayList<>(sortedParams.keySet());
        Collections.sort(keys);
        int index = 0;

        for (String key : keys) {
            Object value = sortedParams.get(key);
            if (isNotEmpty(key) && isNotEmpty(value)) {
                content.append(index == 0 ? "" : "&").append(key).append("=").append(value);
                ++index;
            }
        }
        return content.toString();
    }

    /**
     * 替换手机号中间4位为*
     *
     * @param mobile
     * @return String
     * @Title: replaceMobile
     * @Description: 替换手机号中间4位为*
     * @throw
     */
    public static String replaceMobile(String mobile) {
        return mobile.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * map 排序
     *
     * @param sortedParams
     * @return
     */
    public static Map<String, Object> mapSort(Map<String, ?> sortedParams) {
        Map<String, Object> map = new HashMap<>(16);
        List<String> keys = new ArrayList<>(sortedParams.keySet());
        Collections.sort(keys);
        int index = 0;
        for (String key : keys) {
            Object value = sortedParams.get(key);
            map.put(key, value);
            ++index;
        }
        return map;
    }

    /**
     * 设置Str带默认值
     *
     * @param msg
     * @param defMsg
     * @return
     */
    public static String setStr(String msg, String defMsg) {
        if (StrUtil.isEmpty(msg)) {
            return defMsg;
        }
        return msg;
    }

    /**
     * 下划线字符
     */
    private static final char UNDERLINE = '_';

    /**
     * 字符串下划线转驼峰格式
     *
     * @param param 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String underlineToCamel(String param) {
        if (isEmpty(param)) {
            return "";
        }
        String temp = param.toLowerCase();
        int len = temp.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = temp.charAt(i);
            if (c == UNDERLINE) {
                if (++i < len) {
                    sb.append(Character.toUpperCase(temp.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 字符串驼峰转下划线格式
     *
     * @param param 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String camelToUnderline(String param) {
        if (isEmpty(param)) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c) && i > 0) {
                sb.append(UNDERLINE);
            }
            sb.append(Character.toLowerCase(c));
        }
        return sb.toString();
    }

    private static final String NEW_LINE = "\n";
    private static final String ERROR_MESSAGE_FORMAT = "%s.%s(%s:%d)";

    /**
     * 把异常堆栈信息转化为字符串,同时把所有的errorMessage用\n方式拼接到printStackTrace前面 -> 替代 e.printStackTrace()
     * <p>使用e.printStackTrace()不能作为字符串处理，本方法可以转换e.printStackTrace()为字符串</p>
     * <p><b>printStackTrace</b>信息从<i>Caused by:</i>C开始</p>
     *
     * @param cause
     * @return
     */
    public static String printStackTraceAndMessage(Throwable cause) {
        if (cause != null) {
            StringBuilder msg = new StringBuilder();
            if (isNotEmpty(cause.getMessage())) {
                msg.append(cause.getMessage()).append(NEW_LINE);
            }
            String causedMsg = printStackTrace(cause, (eMessage) -> {
                if (isNotEmpty(eMessage)) {
                    msg.append(eMessage).append(NEW_LINE);
                }
            });
            return msg.append(causedMsg).toString();
        }
        return "";
    }

    /**
     * 把异常堆栈信息转化为字符串 -> 替代 e.printStackTrace()
     * <p>使用e.printStackTrace()不能作为字符串处理，本方法可以转换e.printStackTrace()为字符串</p>
     *
     * @param cause
     * @return
     */
    public static String printStackTrace(Throwable cause) {
        return printStackTrace(cause, (eMessage) -> {
        });
    }

    private static String printStackTrace(Throwable cause, Consumer<String> consumer) {
        if (cause != null) {
            StringBuilder sb = new StringBuilder();
            String cClass = cause.getClass().getName();
            String eMessage = cause.getMessage();
            StackTraceElement[] stackTrace = cause.getStackTrace();
            Throwable caused = cause.getCause();
            while (caused != null) {
                cClass = caused.getClass().getName();
                eMessage = caused.getMessage();
                stackTrace = caused.getStackTrace();
                caused = caused.getCause();
                consumer.accept(eMessage);
            }
            sb.append("Caused by: ").append(cClass).append(": ").append(eMessage).append(NEW_LINE);
            for (StackTraceElement element : stackTrace) {
                sb.append("\tat ");
                sb.append(String.format(ERROR_MESSAGE_FORMAT, element.getClassName(), element.getMethodName(), element.getFileName(), element.getLineNumber()));
                sb.append(NEW_LINE);
            }
            return sb.toString();
        }
        return "";
    }

    /**
     * 不是空对象时执行
     *
     * @param checkObj 判定对象
     * @param consumer 执行器
     * @param <T>      判定对象
     */
    public static <T> void notNullExecute(T checkObj, Consumer<T> consumer) {
        if (null != checkObj) {
            consumer.accept(checkObj);
        }
    }

    /**
     * 不是空内容时执行
     *
     * @param checkObj 判定对象
     * @param consumer 执行器
     * @param <T>      判定对象
     */
    public static <T> void notEmptyExecute(T checkObj, Consumer<T> consumer) {
        if (isNotEmpty(checkObj)) {
            consumer.accept(checkObj);
        }
    }

    private static final List<String> COUNTRY_CODE = Arrays.asList("JPN", "KOR", "THA", "SGP", "CHN", "TWN", "HKG", "MAC", "999");

    /**
     * 国名を国コードに変換
     *
     * @param country JPN：日本、KOR：韓国、THA：タイ、SGP：シンガポール、CHN：中国内陸、TWN：中国台湾、HKG：中国香港、MAC：マカオ、999：その他、０：不明
     * @return code byte <pre>
     * 1     0     1     　0　　　 　　1       1     1     1     1 <br>
     * 日本　韓国　 タイ  シンガポール  中国内陸  台湾　 香港  マカオ  その他
     * <br>
     * 右→左：0位：その他、1位：マカオ、2位：香港、3位：台湾、4位：中国内陸、5位：シンガポール、6位：タイ、7位：韓国、8位：日本
     * <br>  1：当該国で表示、0：当該国表示しない
     * </pre>
     */
    public static byte countryToCodeByte(String country) {
        String code = countryToCode(country);
        return (byte) Integer.parseInt(code, 2);
    }

    /**
     * 国家代码转換成二进制
     *
     * @param country JPN：日本、KOR：韓国、THA：泰国、CHN：中国内陸、SGP：新加坡、TWN：中国台湾、HKG：中国香港、MAC：中国澳门、999：其他、０：不明
     * @return <pre>
     * 1     0     1    0　　　　1       1     1     1     1 <br>
     * 日本　韓国　 泰国  新加坡  中国内陸  台湾　 香港   澳门   其他
     * <br>
     * 右→左：0位：其他、1位：中国澳门、2位：中国香港、3位：中国台湾、4位：中国内陸、5位：新加坡、6位：泰国、7位：韓国、8位：日本
     * <br> 1：在该国表示、0：不表示该国
     * </pre>
     */
    public static String countryToCode(String country) {
        int index = COUNTRY_CODE.indexOf(country);
        if (index == -1) {
            return "000000000";
        }
        int bn = 1 << (COUNTRY_CODE.size() - 1 - index);
        // 转成二进制
        String bs = Integer.toBinaryString(bn);
        // 为了保证长度一致，前面补0
        return String.format("%09d", Integer.parseInt(bs));
    }

    /**
     * 国家代码二进制转国家代码
     *
     * @param countryCode 国家代码二进制转:010000000
     *  <pre>
     *  1     0     1    0　　　　1       1     1     1     1 <br>
     *  日本　韓国　 泰国  新加坡  中国内陸  台湾　 香港   澳门   其他
     *  <br>
     *  右→左：0位：其他、1位：中国澳门、2位：中国香港、3位：中国台湾、4位：中国内陸、5位：新加坡、6位：泰国、7位：韓国、8位：日本
     *  <br> 1：在该国表示、0：不表示该国
     *  </pre>
     * @return JPN：日本、KOR：韓国、THA：泰国、CHN：中国内陸、SGP：新加坡、TWN：中国台湾、HKG：中国香港、MAC：中国澳门、999：其他、０：不明
     */
    public static String getCountryByCode(String countryCode) {
        int i = Integer.parseInt(countryCode, 2);
        int index = Integer.numberOfTrailingZeros(i);
        if (index > COUNTRY_CODE.size()) {
            return "O";
        }
        return COUNTRY_CODE.get(COUNTRY_CODE.size() - 1 - index);
    }

    /**
     * nationalCodeは想定国エリア範囲内存在するかどうか
     * 想定国エリア範囲："JPN", "KOR", "THA", "SGP", "CHN", "TWN", "HKG", "MAC", "999"
     *
     * @param nationCode
     * @return 存在：true；存在しない：false
     */
    public static boolean containsCountry(String nationCode) {
        if (isNotEmpty(nationCode)) {
            return COUNTRY_CODE.contains(nationCode);
        }
        return false;
    }

}
