package com.yexuejc.base.util;

import com.yexuejc.base.pojo.CreateZipFileBean;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ExcludeFileFilter;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 压缩相关
 * <p>依赖于zip4j：<a href='https://github.com/srikanth-lingala/zip4j'>https://github.com/srikanth-lingala/zip4j</a></p>
 *
 * @author maxf
 * @class-name ZipUtil
 * @description 文件压缩
 * @date 2022/11/11 11:09
 */
public class ZipUtil {

    /**
     * 创建压缩文件
     *
     * @param zipFileBean
     */
    public static void createZipFile(CreateZipFileBean zipFileBean) throws ZipException {
        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setCompressionMethod(CompressionMethod.STORE);

        //加密
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);
        ZipFile zipFile = new ZipFile(zipFileBean.getZipFile());
        if (StrUtil.isNotEmpty(zipFileBean.getEncryptPwd())) {
            zipFile = new ZipFile(zipFileBean.getZipFile(), zipFileBean.getEncryptPwd().toCharArray());
        }
        if (StrUtil.isNotEmpty(zipFileBean.getSourceFileName())) {
            //压缩文件
            List<File> filesToAdd = new ArrayList<>(16);
            Map<String, String> fileNamesMap = new HashMap<>(16);
            zipFileBean.getSourceFileName().forEach(it -> {
                String file = zipFileBean.getSourcePath() + File.separator + it;
                filesToAdd.add(new File(file));
                fileNamesMap.put(file, it);
            });
            zipFile.renameFiles(fileNamesMap);
            zipFile.addFiles(filesToAdd, zipParameters);
        } else {
            //压缩目录
            ExcludeFileFilter excludeFileFilter = zipFileBean.getExcludeFileName()::contains;
            zipParameters.setExcludeFileFilter(excludeFileFilter);
            zipFile.addFolder(new File(zipFileBean.getSourcePath()), zipParameters);
        }
    }
}

//checkemun key -> codeMst检索(缓存)
//返回exception(message用区分分割文言和exp)