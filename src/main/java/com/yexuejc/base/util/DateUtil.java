package com.yexuejc.base.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * java.util.Date 时间工具类
 *
 * @author maxf
 * @ClassName DateUtil
 * @Description
 * @date 2018/9/3 15:27
 */
public class DateUtil {
    private DateUtil() {
    }

    public static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    public static DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static String DAY_START_TIME = "00:00:00";
    public static String DAY_END_TIME = "23:59:59";

    /**
     * 取得服务器当前日期
     *
     * @return 当前日期：格式（yyyy-MM-dd）
     */
    static public String currentDate() {
        Date curDate = new Date();
        return DATE_FORMAT.format(curDate);
    }

    /**
     * 取得服务器当前时间
     *
     * @return 当前日期：格式（hh:mm:ss）
     */
    static public String currentTime() {
        Date curDate = new Date();
        return TIME_FORMAT.format(curDate);
    }

    /**
     * 取得服务器当前日期和时间
     *
     * @return 当前日期：格式（yyyy-MM-dd hh:mm:ss）
     */
    static public String currentDateTime() {
        Date curDate = new Date();
        return DATE_TIME_FORMAT.format(curDate);
    }

    /**
     * 比较两个日期大小
     *
     * @param date1 格式 yyyy-MM-dd
     * @param date2 格式 yyyy-MM-dd
     * @return date1>date2返回1;date1=date2返回0;date1<date2返回-1
     * @throws ParseException
     */
    public static byte dateCompare(String date1, String date2) throws ParseException {

        Date d1 = DATE_FORMAT.parse(date1);
        Date d2 = DATE_FORMAT.parse(date2);
        if (d1.before(d2)) {
            return -1;
        } else if (d1.equals(d2)) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * 日期字符串转date
     *
     * @param dateStr 格式：yyyy-MM-dd
     * @return Date 日期
     * @throws ParseException
     * @deprecated 替代参考 {@link #parseDate(String, String)}
     * @see 1.5.2
     */
    @Deprecated
    public static Date str2date(String dateStr) throws ParseException {
        Date date = DATE_FORMAT.parse(dateStr);
        return date;
    }


    /**
     * date转字符串
     *
     * @param date 日期
     * @return String 格式 yyyy-MM-dd
     * @deprecated 替代参考 {@link #formatDate(Date, String)} 或 {@link #formatDate(Date, String, Locale)}
     * @see 1.5.2
     */
    @Deprecated
    public static String date2str(Date date) {
        if (date != null) {
            return DATE_FORMAT.format(date);
        } else {
            return "";
        }
    }


    /**
     * 日期字符串转dateTime
     *
     * @param dateStr 格式：yyyy-MM-dd HH:mm:ss.SSS
     * @return Date 时间
     * @throws ParseException
     * @deprecated 替代参考 {@link #parseDate(String, String)}
     * @see 1.5.2
     */
    @Deprecated
    public static Date str2dateTime(String dateStr) throws ParseException {
        return DATE_TIME_FORMAT.parse(dateStr);
    }

    /**
     * dateTime转字符串
     *
     * @param date 时间
     * @return String 格式：yyyy-MM-dd HH:mm:ss.SSS
     * @deprecated 替代参考 {@link #formatDate(Date, String)} 或 {@link #formatDate(Date, String, Locale)}
     * @see 1.5.2
     */
    @Deprecated
    public static String dateTime2str(Date date) {
        if (date != null) {
            return DATE_TIME_FORMAT.format(date);
        } else {
            return "";
        }
    }

    /**
     * 格式化当前日期为指定格式的字符串。
     *
     * @param dateFormat 日期格式字符串，用于指定日期的输出格式，例如"yyyy-MM-dd HH:mm:ss"。
     * @return 格式化后的当前日期字符串。
     */
    public static String formatDateNow(String dateFormat) {
        return formatDate(new Date(), dateFormat); // 使用系统当前时间生成日期对象，并按指定格式进行格式化。
    }

    /**
     * 解析字符串形式的日期到Date对象。
     *
     * @param dateStr 待解析的日期字符串。
     * @param dateFormat 日期字符串的格式。
     * @return 返回解析后的Date对象，如果解析失败则返回null。
     * @throws ParseException
     */
    public static Date parseDate(String dateStr, String dateFormat) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        // 尝试根据给定的日期格式解析日期字符串
        return sdf.parse(dateStr);
    }

    /**
     * 根据指定的日期格式和地域格式化日期对象为字符串。
     * 如果未指定地域，则默认使用中文地域格式。
     *
     * @param date 需要格式化的日期对象。
     * @param dateFormat 日期格式字符串，例如"yyyy-MM-dd"。
     * @return 格式化后的日期字符串。
     */
    public static String formatDate(Date date, String dateFormat) {
        return formatDate(date, dateFormat, null);
    }

    /**
     * 根据指定的日期格式、地域格式化日期对象为字符串。
     *
     * @param date 需要格式化的日期对象。
     * @param dateFormat 日期格式字符串，例如"yyyy-MM-dd"。
     * @param locale 地域设置，如果为null则默认使用中文地域。
     * @return 格式化后的日期字符串。
     */
    public static String formatDate(Date date, String dateFormat, Locale locale) {
        SimpleDateFormat sdf;
        // 根据是否提供了地域参数来创建SimpleDateFormat实例
        if (StrUtil.isEmpty(locale)) {
            sdf = new SimpleDateFormat(dateFormat, Locale.CHINA);
        } else {
            sdf = new SimpleDateFormat(dateFormat, locale);
        }
        return sdf.format(date);
    }

    /**
     * 获取本周的日期
     *
     * @param dayOfWeek :可用值:Calendar.SUNDAY,Calendar.MONDAY,
     *                  Calendar.TUESDAY,Calendar.WEDNESDAY,Calendar.THURSDAY,
     *                  Calendar.FRIDAY,Calendar.SATURDAY
     * @return
     */
    public static String getCurrentWeek(int dayOfWeek) {
        Calendar calendar = Calendar.getInstance(Locale.CHINA);
        if (Calendar.SUNDAY == dayOfWeek) {
            calendar.add(Calendar.WEEK_OF_MONTH, 1);
        }
        calendar.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return DATE_FORMAT.format(calendar.getTime());
    }

    /**
     * 两个日期相减
     *
     * @param date1
     * @param date2
     * @param flag  : 'y':得出相差年数,'M':相差月数,'d':相差天数,'h':相差小时数
     *              'm':相差分钟数,'s':相差秒数,其他:相差毫秒数
     * @return
     */
    public static long dateMinus(Date date1, Date date2, char flag) {
        long msMinus = date1.getTime() - date2.getTime();
        switch (flag) {
            case 'y':
                return msMinus / (365L * 24L * 60L * 60L * 1000L);
            case 'M':
                return msMinus / (30L * 24L * 60L * 60L * 1000L);
            case 'd':
                return msMinus / (24L * 60L * 60L * 1000L);
            case 'h':
                return msMinus / (60L * 60L * 1000L);
            case 'm':
                return msMinus / (60L * 1000L);
            case 's':
                return msMinus / 1000L;
            default:
                return msMinus;
        }
    }

    /**
     * 一个日期加上xx天,返回加法之后的日期
     *
     * @param date
     * @param days
     * @return
     */
    public static Date datePlus(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + days);
        return calendar.getTime();
    }

    /**
     * 获取UTC（格林威治，标准）时间
     *
     * @param date
     * @return
     */
    public static Date convertUTC(Date date) {
        return convertTimezone(date, null, "UTC");
    }

    /**
     * 时区转换(当前时区)
     *
     * @param date   时间
     * @param zoneId 转换时区
     * @return
     */
    public static Date convertTimezone(Date date, String zoneId) {
        return convertTimezone(date, null, zoneId);
    }

    /**
     * 时区转换
     *
     * @param date          时间
     * @param currentZoneId 当前时间的时区
     * @param targetZoneId  转换时区
     * @return
     */
    public static Date convertTimezone(Date date, String currentZoneId, String targetZoneId) {
        ZoneId currentZone;
        if (currentZoneId == null) {
            currentZone = ZoneId.systemDefault();
        } else {
            currentZone = ZoneId.of(currentZoneId);
        }
        ZoneId targetZone = ZoneId.of(targetZoneId);
        return Date.from(date.toInstant().atZone(currentZone).withZoneSameInstant(targetZone).toInstant());
    }

/*    public static void main(String[] args) throws ParseException {
        System.out.println(DateUtil.currentDate());
        System.out.println(DateUtil.currentTime());
        System.out.println(DateUtil.currentDateTime());
        System.out.println(DateUtil.dateCompare("2024-01-25","2025-01-01"));
        System.out.println(DateUtil.str2date("2024-01-25"));
        System.out.println(DateUtil.date2str(new Date()));
        System.out.println(DateUtil.str2dateTime("2024-04-08 11:01:39.361"));
        System.out.println(DateUtil.dateTime2str(new Date()));
        System.out.println(DateUtil.getCurrentWeek(1));
        System.out.println(DateUtil.dateMinus(DateUtil.datePlus(new Date(),50),new Date(),'M'));
        System.out.println(DateUtil.convertUTC(new Date()));
        System.out.println(DateUtil.convertTimezone(new Date(),"UTC","Asia/Shanghai"));
        System.out.println(DateUtil.convertTimezone(new Date(),"UTC"));
    }*/
}
