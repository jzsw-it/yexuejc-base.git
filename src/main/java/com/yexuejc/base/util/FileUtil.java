package com.yexuejc.base.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.CRC32;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.yexuejc.base.annotation.CsvToBean;
import com.yexuejc.base.pojo.ReadFileBean;
import io.jsonwebtoken.lang.Assert;

/**
 * 文件工具类
 *
 * @author maxf:yexue
 * @className FileUtil
 * @description 工具类
 * @time 2017年11月3日 下午3:12:49
 */
public class FileUtil {
    private static final Logger logger = Logger.getLogger(FileUtil.class.getName());

    private FileUtil() {
    }


    private static final String NEW_LINE = "\n";
    private static final String CONSTANT_DOT = ".";
    private static final String TYPE_TAR_GZ = ".tar.gz";
    private static final String TYPE_CSV = ".csv";
    private static final String TAR_GZ = "tar.gz";

    /**
     * 获取文件类型：不适合所有
     * <p>
     * 根据文件名称截取.后的文件格式
     * </p>
     *
     * @param fileName
     * @return
     */
    public static String getFileType(String fileName) {
        try {
            if (fileName.lastIndexOf(TYPE_TAR_GZ) > 0) {
                return TAR_GZ;
            }
            return fileName.substring(fileName.lastIndexOf(CONSTANT_DOT) + 1);
        } catch (Exception e) {
            logger.severe("file doesn't exist or is not a file");
        }
        return null;
    }

    /**
     * 判断文件是否存在
     *
     * @param filePath
     * @return false 文件不存在；true 文件存在
     */
    public static boolean isFileExist(String filePath) {
        if (StrUtil.isEmpty(filePath)) {
            return false;
        }
        File file = new File(filePath);
        return file.exists() && !file.isDirectory();
    }

    /**
     * 判断文件是否存在,不存在就创建一个空的
     *
     * @param file
     */
    public static void judeFileExists(File file) {
        if (file.exists()) {
            logger.severe("file exists");
        } else {
            logger.info("file not exists, create it ...");
            try {
                boolean b = file.createNewFile();
                if (b) {
                    logger.info("file create success");
                } else {
                    logger.severe("file create fail");
                }
            } catch (IOException e) {
                logger.log(Level.WARNING, "file create fail", e);
            }
        }
    }

    /**
     * <pre>
     * 判断文件夹是否存在, 不存在创建【采用mkdirs】
     * 相关解释：
     * 1、File类的createNewFile根据抽象路径创建一个新的空文件，当抽象路径制定的文件存在时，创建失败
     * 2、File类的mkdir方法根据抽象路径创建目录
     * 3、File类的mkdirs方法根据抽象路径创建目录，包括创建必需但不存在的父目录
     * 4、File类的createTempFile方法创建临时文件，可以制定临时文件的文件名前缀、后缀及文件所在的目录，如果不指定目录，则存放在系统的临时文件夹下。
     * 5、除mkdirs方法外，以上方法在创建文件和目录时，必须保证目标文件不存在，而且父目录存在，否则会创建失败
     * </pre>
     *
     * @return 创建成功、失败
     */
    public static boolean judeDirExists(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                logger.severe("dir exists");
            } else {
                logger.severe("the same name file exists, can not create dir");
            }
        } else {
            logger.info("dir not exists, create it ...");
            return file.mkdirs();
        }
        return false;
    }

    /**
     * 获取文件sha1
     *
     * @param file
     * @return
     */
    public static String sha1(File file) {
        return getDigest(file, "SHA-1");
    }


    /**
     * 文件md5
     *
     * @param file
     * @return
     */
    public static String md5(File file) {
        return getDigest(file, "MD5");
    }

    /**
     * 获取文件的散列值
     * @param file
     * @param digestCode
     * @return
     */
    private static String getDigest(File file, String digestCode) {
        try (FileInputStream in = new FileInputStream(file)) {
            MessageDigest digest = MessageDigest.getInstance(digestCode);
            FileChannel channel = in.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024); // 1MB 缓冲区
            // 读取文件内容并更新 MessageDigest
            while (channel.read(buffer) != -1) {
                buffer.flip(); // 将 Buffer 从写模式切换到读模式
                digest.update(buffer); // 更新 MessageDigest
                buffer.clear(); // 清空 Buffer
            }
            // 计算最终的 SHA-1 散列值
            byte[] sha1Bytes = digest.digest();
            // 将字节数组转换为十六进制字符串
            StringBuilder sha1Builder = new StringBuilder();
            for (byte b : sha1Bytes) {
                sha1Builder.append(String.format("%02x", b));
            }
            return sha1Builder.toString();
        } catch (NoSuchAlgorithmException e) {
            logger.log(Level.SEVERE, "system algorithm error.", e);
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "file doesn't exist or is not a file", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "The operation file is an IO exception.", e);
        }
        return null;
    }

    /**
     * 获取文件CRC32码
     *
     * @return 获取失败返回-1
     */
    public static long crc32(File file) {
        CRC32 crc32 = new CRC32();
        // MessageDigest.get
        try (FileInputStream fileInputStream = new FileInputStream(file);) {
            byte[] buffer = new byte[1024 * 1024];
            int length;
            while ((length = fileInputStream.read(buffer)) != -1) {
                crc32.update(buffer, 0, length);
            }
            return crc32.getValue();
        } catch (FileNotFoundException e) {
            logger.log(Level.SEVERE, "file doesn't exist or is not a file", e);
            return -1;
        } catch (IOException e) {
            logger.log(Level.SEVERE, "The operation file is an IO exception.", e);
            return -1;
        }
    }

    /**
     * 获取文件base64
     *
     * @param file
     * @return
     */
    public static String base64ToStr(File file) {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(file.getPath()));
            return Base64.getEncoder().encodeToString(bytes);
        } catch (IOException e) {
            logger.severe("The operation file is an IO exception.");
        }
        return null;
    }

    /**
     * base64转文件
     * <p>
     * <i>
     * 文件转base64请使用 {@link FileUtil#base64ToStr(File)}
     * </i>
     *
     * @param decode   {@link FileUtil#base64ToStr(File)} 的结果
     * @param fileName 文件名称（包含路径）
     * @return 返回保存地址
     */
    public static String base64ToFile(String decode, String fileName) throws IOException {
        return base64ToFile(Base64.getDecoder().decode(decode.getBytes()), fileName);
    }

    /**
     * base64转文件
     * <p>
     * <i>
     * 文件转base64请使用 {@link FileUtil#base64ToStr(File)}}
     * </i>
     *
     * @param decode   baseByte
     * @param fileName 文件名称（包含路径）
     * @return 返回保存地址
     */
    public static String base64ToFile(byte[] decode, String fileName) throws IOException {
        try (FileOutputStream out = new FileOutputStream(fileName)) {
            out.write(decode);
        }
        return fileName;
    }

    /**
     * 获取文件大小 ：直接返回大小
     *
     * @param path 文件地址
     * @return f.length()
     */
    public static long size(Path path) throws IOException {
        if (Files.exists(path) && Files.isRegularFile(path)) {
            return Files.size(path);
        } else {
            logger.info("file doesn't exist or is not a file");
        }
        return 0;
    }

    /**
     * 字符串（csv格式）转 对象
     *
     * @param data      转换的字符串 如
     *                  <p> ------------ </p>
     *                  <p> id,name,age </p>
     *                  <p> 1,zhangsan,18   </p>
     *                  <p> 2,lisi,20 </p>
     *                  <p> ------------ </p>
     * @param cls       需要转换的对象,含有<b>id,name,age</b>字段
     * @param delimiter 分隔符
     * @param <I>
     * @return
     */
    public static <I> List<I> readCsv(String data, Class<I> cls, char delimiter) throws IOException {
        CsvMapper csvMapper = new CsvMapper();
        CsvSchema csvSchema = CsvSchema.emptySchema().withHeader().withColumnSeparator(delimiter);
        MappingIterator<I> orderLines = csvMapper.readerFor(cls).with(csvSchema).readValues(data);
        return orderLines.readAll();
    }

    /**
     * 读取csv文件
     *
     * @param csvFilePath 文件地址
     * @param cls         读取转化的对象
     * @param header      解析列对应的java字段；用delimiter分割
     * @param hasHeader   csv文件中第一行是否是header
     * @param delimiter   分隔符.默认【,】
     * @param <I>
     * @return
     */
    public static <I> List<I> readCsv(final String csvFilePath, Class<I> cls, boolean hasHeader, String header, char delimiter) {
        if (!isFileExist(csvFilePath)) {
            throw new RuntimeException(String.format("解析用的csv： [%s] 文件不存在。", csvFilePath));
        }
        if (StrUtil.isEmpty(delimiter)) {
            delimiter = ',';
        }
        try {
            File csvFile = new File(csvFilePath);
            CsvMapper csvMapper = new CsvMapper();
            CsvSchema.Builder builder = CsvSchema.builder();
            if (StrUtil.isNotEmpty(header)) {
                builder.addColumns(Arrays.asList(header.split(String.valueOf(delimiter))), CsvSchema.ColumnType.STRING);
            }
            CsvSchema csvSchema = builder.build().withColumnSeparator(delimiter).withSkipFirstDataRow(hasHeader).withStrictHeaders(hasHeader).withComments();

            MappingIterator<I> recordIterator = csvMapper.readerWithTypedSchemaFor(cls).with(csvSchema).readValues(csvFile);
            return recordIterator.readAll();
        } catch (IOException e) {
            throw new RuntimeException("[" + csvFilePath + "] 文件解析失败。", e);
        }
    }

    /**
     * 分段读取大文件（不限格式）
     *
     * @param filePath         文件路径
     * @param readFileBean 分段每次读取的bean 初始值需要设置每次读取的行数
     * @param <T>          读取结果类型bean
     * @return 文件分页读取内容（自定义处理后）及读取信息
     */
    public static <T> ReadFileBean<T> readBigFile(String filePath, ReadFileBean<T> readFileBean, Function<List<String>, List<T>> readAfter) throws IOException {
        if (!isFileExist(filePath)) {
            throw new FileNotFoundException(String.format("[%s]文件不存在。", filePath));
        }
        List<String> datas = new ArrayList<>();
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(new File(filePath), "r")) {
            if (readFileBean.getPointer() < 0) {
                readFileBean.setPointer(0);
            }
            randomAccessFile.seek(readFileBean.getPointer());
            readFileBean.setFileLength(randomAccessFile.length());
            int row = 0;
            String line;
            while ((line = randomAccessFile.readLine()) != null && row <= readFileBean.getReadRowNum()) {
                row++;
                readFileBean.setPointer(randomAccessFile.getFilePointer());
                datas.add(readFileBean.lineScavenge(charsetDecode(line, readFileBean.getReadCharset())));
            }
        }
        if (StrUtil.isEmpty(datas)) {
            //无数据
            return readFileBean.setDatas(new ArrayList<>());
        }
        List<T> dataList = readAfter.apply(datas);
        readFileBean.setDatas(dataList);
        return readFileBean;
    }

    /**
     * 分段读取大文件(不解析)
     *
     * @param csvFilePath         文件路径
     * @param readFileBean 分段每次读取的bean 初始值需要设置每次读取的行数
     * @return 文件分页读取内容（每行为一个String对象）及读取信息
     */
    public static ReadFileBean<String> readBigFile(String csvFilePath, ReadFileBean<String> readFileBean) throws IOException {
        return readBigFile(csvFilePath, readFileBean, (datas) -> datas);
    }

    /**
     * 分段读取大文件(CSV格式)
     *
     * @param csvFilePath         文件路径
     * @param readFileBean 分段每次读取的bean 初始值需要设置每次读取的行数
     * @param <T>          读取结果类型bean
     * @return 文件分页读取内容（转bean后）及读取信息
     */
    public static <T> ReadFileBean<T> readBigFile(String csvFilePath, ReadFileBean<T> readFileBean, Class<T> readCls) throws IOException {
        if (!csvFilePath.endsWith(TYPE_CSV)) {
            throw new IOException(String.format("[%s]文件不是CSV文件格式。", csvFilePath));
        }
        return readBigFile(csvFilePath, readFileBean, (datas) -> {
            //csv文件处理
            com.yexuejc.base.pojo.CsvToBean csvToBean = getCsvToBean(readCls);
            readFileBean.setHeader(csvToBean.getHeader());
            if (csvToBean.hasHeader()) {
                //文件存在header,设置header优先,没设置使用文件的
                if (StrUtil.isNotEmpty(csvToBean.getHeader())) {
                    //替换header
                    datas.remove(0);
                    datas.add(0, csvToBean.getHeader());
                } else {
                    readFileBean.setHeader(datas.get(0));
                }
            } else {
                //文件不存在header，使用设置的
                datas.add(0, csvToBean.getHeader());
            }
            try {
                return readCsv(String.join(NEW_LINE, datas), readCls, csvToBean.getDelimiter());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

    }

    /**
     * 获取csv的header,使用注解{@link CsvToBean}
     *
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> com.yexuejc.base.pojo.CsvToBean getCsvToBean(Class<T> cls) {
        CsvToBean annotation = cls.getAnnotation(CsvToBean.class);
        Assert.notNull(annotation, cls + "类上需要添加注解@CsvToBean，并指定header。");
        return new com.yexuejc.base.pojo.CsvToBean(annotation.header(), annotation.delimiter(), annotation.hasHeader());
    }

    /**
     * 把字符串data按照指定编码解码
     *
     * @param data    解码字符串
     * @param charset 字符编码
     * @return
     */
    public static String charsetDecode(String data, Charset charset) {
        char[] chars = data.toCharArray();
        byte[] result = new byte[chars.length];
        for (int i = 0; i < chars.length; i++) {
            result[i] = (byte) chars[i];
        }
        return new String(result, charset);
    }
}
