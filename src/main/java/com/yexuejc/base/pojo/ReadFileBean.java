package com.yexuejc.base.pojo;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;

/**
 * @author maxf
 * @class-name ReadFileBean
 * @description 分段读取大文件
 * @date 2022/5/16 21:53
 */
public class ReadFileBean<T> {
    /**
     * 开始行数
     */
    private int startRowNum;
    /**
     * 结束行数
     */
    private int endRowNum;

    /**
     * 每次读取的行数
     */
    private int readRowNum;
    /**
     * 开始行到结束行的数据
     */
    private List<T> datas;
    /**
     * 文件指针位置,默认0，开始位置
     */
    private long pointer = 0;
    /**
     * 文件的length
     */
    private long fileLength;
    /**
     * csv文件存在header
     */
    private String header;
    private Charset readCharset = StandardCharsets.UTF_8;
    public Function<String, String> lineScavenger = t -> t;

    public ReadFileBean() {
        this.readRowNum = Integer.MAX_VALUE;
        this.startRowNum = 1;
        this.endRowNum = 0;
    }

    public ReadFileBean(int readRow) {
        this.readRowNum = readRow;
        this.startRowNum = 1;
        this.endRowNum = 0;
    }

    public Charset getReadCharset() {
        return readCharset;
    }

    public ReadFileBean<T> setReadCharset(Charset readCharset) {
        this.readCharset = readCharset;
        return this;
    }

    public String getHeader() {
        return header;
    }

    public ReadFileBean<T> setHeader(String header) {
        this.header = header;
        return this;
    }

    public int getReadRowNum() {
        return readRowNum;
    }

    public int getStartRowNum() {
        return startRowNum;
    }

    public int getEndRowNum() {
        return endRowNum;
    }

    public List<T> getDatas() {
        return datas;
    }

    public ReadFileBean<T> setDatas(List<T> datas) {
        this.datas = datas;
        this.startRowNum = this.endRowNum + 1;
        this.endRowNum += this.readRowNum;
        return this;
    }

    public long getPointer() {
        return pointer;
    }

    public ReadFileBean<T> setPointer(long pointer) {
        this.pointer = pointer;
        return this;
    }

    public long getFileLength() {
        return fileLength;
    }

    public ReadFileBean<T> setFileLength(long fileLength) {
        this.fileLength = fileLength;
        return this;
    }

    public String lineScavenge(String lineData) {
        return this.lineScavenger.apply(lineData);
    }

    public boolean hasNext() {
        return this.fileLength > this.pointer;
    }
}
