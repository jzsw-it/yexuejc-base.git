package com.yexuejc.base.pojo;

import java.util.List;

/**
 * 压缩文件的参数设定
 *
 * @author maxf
 * @class-name CreateZipFileBean
 * @description 压缩文件参考 {@link com.yexuejc.base.util.ZipUtil}
 * @date 2022/11/11 10:30
 */
public class CreateZipFileBean {
    /**
     * 压缩的源文件列表，为空表示压缩整个目录
     */
    private List<String> sourceFileName;
    /**
     * 当压缩文件夹时，需要排除的文件。压缩文件时（该参数无效）
     */
    private List<String> excludeFileName;
    /**
     * 压缩源文件的目录
     */
    private String sourcePath;
    /**
     * 生成压缩文件路径（全路径+压缩文件名）
     */
    private String zipFile;
    /**
     * 加密密码
     */
    private String encryptPwd;

    private String writeCharsetName = "UTF-8";

    public List<String> getSourceFileName() {
        return sourceFileName;
    }

    public String getEncryptPwd() {
        return encryptPwd;
    }

    public CreateZipFileBean setEncryptPwd(String encryptPwd) {
        this.encryptPwd = encryptPwd;
        return this;
    }

    public CreateZipFileBean setSourceFileName(List<String> sourceFileName) {
        this.sourceFileName = sourceFileName;
        return this;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public CreateZipFileBean setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
        return this;
    }

    public String getZipFile() {
        return zipFile;
    }

    public CreateZipFileBean setZipFile(String zipFile) {
        this.zipFile = zipFile;
        return this;
    }

    public String getWriteCharsetName() {
        return writeCharsetName;
    }

    public CreateZipFileBean setWriteCharsetName(String writeCharsetName) {
        this.writeCharsetName = writeCharsetName;
        return this;
    }

    public List<String> getExcludeFileName() {
        return excludeFileName;
    }

    public CreateZipFileBean setExcludeFileName(List<String> excludeFileName) {
        this.excludeFileName = excludeFileName;
        return this;
    }
}
