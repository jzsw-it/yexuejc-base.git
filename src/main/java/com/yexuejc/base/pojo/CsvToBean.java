package com.yexuejc.base.pojo;

/**
 * {@link com.yexuejc.base.annotation.CsvToBean}
 * 读取csv文件用
 * @author yexuejc
 * @class-name CsvToBean
 * @description
 * @date 2023/6/6 18:15
 */
public class CsvToBean {

    /**
     * 在类头上设置csv格式的header
     * csv文件和java bean的映射
     */
    private String header;

    /**
     * csv文件的分割符号
     */
    private char delimiter;

    /**
     * CSV文件是否有header
     */
    private boolean hasHeader;

    public CsvToBean(String header, char delimiter, boolean hasHeader) {
        this.header = header;
        this.delimiter = delimiter;
        this.hasHeader = hasHeader;
    }

    public String getHeader() {
        return header;
    }

    public CsvToBean setHeader(String header) {
        this.header = header;
        return this;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public CsvToBean setDelimiter(char delimiter) {
        this.delimiter = delimiter;
        return this;
    }

    public boolean hasHeader() {
        return hasHeader;
    }

    public CsvToBean setHasHeader(boolean hasHeader) {
        this.hasHeader = hasHeader;
        return this;
    }
}
