package com.yexuejc.base.constant;

/**
 * @author yexuejc
 * @class-name DateConsts
 * @description
 * @date 2023/05/17 13:45
 */
public class DateConsts {
    public static final String BAR = "-";
    public static final CharSequence DATE_KEY_AM = "AM";
    public static final CharSequence DATE_KEY_PM = "PM";
    public static final String DATE_TIMESTAMP_LINUX = "M/dd/yy, h:mm a";
    public static final CharSequence SLASH = "/";
    public static final CharSequence COLON = ":";
    public static final String DATE_YYYY_MM_DD_SLASH = "yyyy/MM/dd";
    public static final CharSequence DATE_KEY_T = "T";
    public static final String DATE_KEY_Z = "Z";
    public static final String DATE_TIMESTAMP = "yyyy/MM/dd H:m:s";
    public static final String DATE_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
}
