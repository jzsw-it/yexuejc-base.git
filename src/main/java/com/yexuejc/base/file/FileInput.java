package com.yexuejc.base.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;

/**
 * 提供读取IO流方法合集
 * <p>读取速度快慢顺序：</p>
 * 1. {@link #read4Buffer(InputStream, Charset)} <br>
 * 2. {@link #read4IOUtilsCopy(InputStream, Charset)} <br>
 * 3. {@link #read4ByteStreams(InputStream, Charset)} <br>
 * 4. {@link #read4Byte(InputStream, Charset)} <br>
 * 5. {@link #read4StringBuilder(InputStream, Charset)} <br>
 * 6. {@link #read4BufferedReaderParallel(InputStream, Charset, String)}<br>
 * 7. {@link #read4BufferedReader(InputStream, Charset, String)}<br>
 * 8. {@link #read4ScannerA(InputStream)}<br>
 * 9. {@link #read4BufferIO(InputStream, Charset)}<br>
 * 10. {@link #read4IOUtils(InputStream, Charset)}<br>
 * 11. {@link #read4ScannerZ(InputStream)}<br>
 * 12. {@link #read4CharStreams(InputStream, Charset)}<br>
 */
public class FileInput {
    /**
     * 读取IO流内容：byte方式
     *
     * @param inputStream
     * @param charset     编码：默认 {@link Charset#defaultCharset()}
     * @return
     * @throws IOException
     */
    public static String read4Byte(InputStream inputStream, Charset charset) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);
        return new String(bytes, charset == null ? Charset.defaultCharset() : charset);
    }

    /**
     * 读取IO流内容：BufferedReader方式
     *
     * @param inputStream
     * @param charset       编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @param lineSeparator 换行方式：默认跟随系统 {@link System#lineSeparator()}
     * @return
     */
    public static String read4BufferedReader(InputStream inputStream, Charset charset, String lineSeparator) {
        return new BufferedReader(
                new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset)
        ).lines().collect(Collectors.joining(lineSeparator == null ? System.lineSeparator() : lineSeparator));
    }

    /**
     * 读取IO流内容：BufferedReader 并行方式
     *
     * @param inputStream
     * @param charset       编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @param lineSeparator 换行方式：默认跟随系统 {@link System#lineSeparator()}
     * @return
     */
    public static String read4BufferedReaderParallel(InputStream inputStream, Charset charset, String lineSeparator) {
        return new BufferedReader(
                new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset)
        ).lines().parallel()
                .collect(Collectors.joining(lineSeparator == null ? System.lineSeparator() : lineSeparator));
    }

    /**
     * 读取IO流内容：Scanner A方式
     *
     * @param inputStream
     * @return
     */
    public static String read4ScannerA(InputStream inputStream) {
        Scanner s = new Scanner(inputStream).useDelimiter("\\A");
        String str = s.hasNext() ? s.next() : "";
        return str;
    }

    /**
     * 读取IO流内容：Scanner Z方式
     *
     * @param inputStream
     * @return
     */
    public static String read4ScannerZ(InputStream inputStream) {
        return new Scanner(inputStream).useDelimiter("\\Z").next();
    }

    /**
     * 读取IO流内容：StringBuilder方式
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     */
    public static String read4StringBuilder(InputStream inputStream, Charset charset) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    /**
     * 读取IO流内容：ByteArrayOutputStream方式
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     */
    public static String read4Buffer(InputStream inputStream, Charset charset) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString(charset == null ? Charset.defaultCharset().name() : charset.name());
    }

    /**
     * 读取IO流内容：BufferedInputStream+ByteArrayOutputStream方式
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     */
    public static String read4BufferIO(InputStream inputStream, Charset charset) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(inputStream);
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int result = bis.read();
        while (result != -1) {
            buf.write((byte) result);
            result = bis.read();
        }
        return buf.toString(charset == null ? Charset.defaultCharset().name() : charset.name());
    }

    /**
     * 读取IO流内容： 依赖于commons-io:commons-io {@link IOUtils#copy(Reader, OutputStream, Charset)}
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     * @throws IOException
     */
    public static String read4IOUtilsCopy(InputStream inputStream, Charset charset) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, charset == null ? Charset.defaultCharset() : charset);
        return writer.toString();
    }

    /**
     * 读取IO流内容： 依赖于commons-io:commons-io {@link IOUtils#toString(InputStream, Charset)}
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     * @throws IOException
     */
    public static String read4IOUtils(InputStream inputStream, Charset charset) throws IOException {
        return IOUtils.toString(inputStream, charset == null ? Charset.defaultCharset() : charset);
    }

    /**
     * 读取IO流内容： 依赖于com.google.guava:guava {@link com.google.common.io.CharStreams#toString(Readable)}
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     * @throws IOException
     */
    public static String read4CharStreams(InputStream inputStream, Charset charset) throws IOException, ClassNotFoundException {
        try {
            Class<?> charStreamsClass = Class.forName("com.google.common.io.CharStreams");
            Method toStringMethod = charStreamsClass.getMethod("toString", InputStreamReader.class);
            return (String) toStringMethod.invoke(null, new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset));
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("缺少依赖，请引入Guava");
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("com.google.common.io.CharStreams.toString调用失败，请检查Guava版本", e);
        }
//        return com.google.common.io.CharStreams.toString(new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset));
    }

    /**
     * 读取IO流内容： 依赖于com.google.guava:guava {@link com.google.common.io.ByteStreams#toByteArray(InputStream)}
     *
     * @param inputStream
     * @param charset     编码：默认跟随系统 {@link Charset#defaultCharset()}
     * @return
     * @throws IOException
     */
    public static String read4ByteStreams(InputStream inputStream, Charset charset) throws IOException, ClassNotFoundException {
        try {
            Class<?> charStreamsClass = Class.forName("com.google.common.io.ByteStreams");
            Method toStringMethod = charStreamsClass.getMethod("toByteArray", InputStreamReader.class);
            return (String) toStringMethod.invoke(null, new InputStreamReader(inputStream, charset == null ? Charset.defaultCharset() : charset));
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("缺少依赖，请引入Guava");
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("com.google.common.io.ByteStreams.toByteArray调用失败，请检查Guava版本", e);
        }
//        return new String(com.google.common.io.ByteStreams.toByteArray(inputStream), charset == null ? Charset.defaultCharset() : charset);
    }
}


    /*public static void main(String[] args) {
        long size = FileUtil.size(new File("E:\\OS\\deepin-15.6-amd64\\DeepinCloudPrintServerInstaller_1.0.0.1.exe"));
        System.out.println(size);
        System.out.println(1024 * 1024 * 5);
        if (size > 1024 * 1024 * 5) {
            System.out.println("文件最大5M");
            return;
        }

        long s1 = fileSize(new File("E:\\OS\\cn_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063766.iso"));
        System.out.println(s1);
        long s2 = fileSize4Stream(new File("E:\\OS\\cn_windows_10_consumer_editions_version_1803_updated_march_2018_x64_dvd_12063766.iso"));
        System.out.println(s2);

        String s1 = base64(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        System.out.println(s1);

        String s = sha1(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        String s2 = sha1ByBigFile(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        System.out.println(s);
        System.out.println(s2);


        String md5 = md5(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        String md52 = md5ByBigFile(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        System.out.println(md5);
        System.out.println(md52);


        String crc32 = crc32(new File("C:\\Users\\Administrator\\Desktop\\a.html"));
        System.out.println(crc32);
    }*/
