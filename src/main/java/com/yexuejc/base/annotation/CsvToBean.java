package com.yexuejc.base.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 设置csv header
 *
 * @author MAXF-MAC
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CsvToBean {

    /**
     * 在类头上设置csv格式的header
     * csv文件和java bean的映射
     */
    String header();

    /**
     * csv文件的分割符号
     */
    char delimiter() default ',';

    /**
     * CSV文件是否有header
     */
    boolean hasHeader() default false;
}
