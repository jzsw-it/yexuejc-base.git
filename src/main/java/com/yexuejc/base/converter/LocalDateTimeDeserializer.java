package com.yexuejc.base.converter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.yexuejc.base.constant.DateConsts;
import com.yexuejc.base.util.StrUtil;

/**
 * json转LocalDateTime
 *
 * @author yexuejc
 * @date 2022/10/08
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        String timeString = jsonParser.getValueAsString();
        if (StrUtil.isEmpty(timeString)) {
            return null;
        }
        if (timeString.contains(DateConsts.DATE_KEY_T)) {
            return LocalDateTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } else if (timeString.contains(DateConsts.DATE_KEY_AM)
                || timeString.contains(DateConsts.DATE_KEY_PM)) {
            return LocalDateTime.parse(timeString,
                    DateTimeFormatter.ofPattern(DateConsts.DATE_TIMESTAMP_LINUX, Locale.ENGLISH));
        } else if (timeString.endsWith(DateConsts.DATE_KEY_Z)) {
            return LocalDateTime.parse(timeString, DateTimeFormatter.ISO_INSTANT);
        } else if (timeString.contains(DateConsts.SLASH)) {
            return LocalDateTime.parse(timeString, DateTimeFormatter.ofPattern(DateConsts.DATE_TIMESTAMP));
        } else {
            return LocalDateTime.parse(timeString,
                    DateTimeFormatter.ofPattern(DateConsts.DATE_YYYY_MM_DD_HH_MM_SS));
        }
    }

}
