package com.yexuejc.base.converter;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;

/**
 * 反序列化中内容为空时，返回Integer为空
 * <p>使用方式：@JsonDeserialize(using = IntegerNullValueDeserializer.class)</p>
 * @author: yexuejc
 * @date: 2024/4/15 18:08
 */
public class IntegerNullValueDeserializer extends StdScalarDeserializer<Integer> {
    public IntegerNullValueDeserializer() {
        super(Integer.class);
    }

    @Override
    public Integer deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = p.getValueAsString();
        if (isInteger(value)) {
            return super._parseInteger(p, ctxt, Integer.class);
        } else {
            return null;
        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}