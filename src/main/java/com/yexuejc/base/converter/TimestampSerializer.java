package com.yexuejc.base.converter;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.yexuejc.base.constant.DateConsts;

/**
 * Timestamp转json
 *
 * @author yexuejc
 * @date 2022/10/08
 */
public class TimestampSerializer extends JsonSerializer<Timestamp> {
    @Override
    public void serialize(Timestamp timestamp, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException {
        SimpleDateFormat df = new SimpleDateFormat(DateConsts.DATE_TIMESTAMP);
        jsonGenerator.writeString(df.format(timestamp));
    }
}