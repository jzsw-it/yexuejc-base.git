package com.yexuejc.base.converter;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.yexuejc.base.constant.DateConsts;
import com.yexuejc.base.util.StrUtil;

/**
 * json转LocalDate
 *
 * @author yexuejc
 * @date 2022/10/08
 */
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        String timeString = jsonParser.getValueAsString();
        if (StrUtil.isEmpty(timeString)) {
            return null;
        }
        if (timeString.contains(DateConsts.BAR)) {
            return LocalDate.parse(timeString, DateTimeFormatter.ISO_DATE);
        } else if (timeString.contains(DateConsts.DATE_KEY_AM)
                || timeString.contains(DateConsts.DATE_KEY_PM)) {
            return LocalDate.parse(timeString,
                    DateTimeFormatter.ofPattern(DateConsts.DATE_TIMESTAMP_LINUX, Locale.ENGLISH));
        } else if (timeString.contains(DateConsts.SLASH) && timeString.contains(DateConsts.COLON)) {
            return LocalDate.parse(timeString.substring(0, 10),
                    DateTimeFormatter.ofPattern(DateConsts.DATE_YYYY_MM_DD_SLASH));
        } else if (timeString.contains(DateConsts.SLASH)) {
            return LocalDate.parse(timeString, DateTimeFormatter.ofPattern(DateConsts.DATE_YYYY_MM_DD_SLASH));
        } else {
            return LocalDate.parse(timeString, DateTimeFormatter.BASIC_ISO_DATE);
        }
    }
}