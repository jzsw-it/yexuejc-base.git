package com.yexuejc.base.converter;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * <pre>
 * json中的“”转String对象时值为null
 * 例子：{str:""} ->
 * JsonBean{
 *  public String str;
 * }
 * str的值为null [不使用{@link StringDeserializer},str的值为“”]
 * </pre>
 *
 * <pre>
 *     使用方式：
 *     jsonMapper.registerModule(new SimpleModule().addDeserializer(String.class, new StringDeserializer()));
 * </pre>
 *
 * @author yexuejc
 * @date 2022/10/08
 */
public class StringDeserializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {
        String timeString = jsonParser.getValueAsString();
        if ("".equals(timeString)) {
            return null;
        }
        return timeString;
    }
}